﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using NetworkCommsDotNet;

namespace NetChatClient
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (NetworkComms.SendReceiveObject<string, bool>("CheckName", Networking.ToIPAddress(textBox1.Text).ToString(), 2014, "CheckNameR", 30000, textBox2.Text) == true)
                {
                    ChatForm cf = new ChatForm(textBox1.Text, textBox2.Text);
                    cf.Show();
                    this.Hide();
                }
            }
            catch {
                MessageBox.Show("Connection Failed");
            }
        }
    }
}
