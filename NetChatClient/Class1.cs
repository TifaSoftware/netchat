﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace NetChatClient
{

    public class ChatMessage
    {
        public string Username;

        public string Message;

        /// <summary>
        /// Constructor for Chat Message
        /// </summary>
        /// <param name="name">Username associated with the message.</param>
        /// <param name="msg">Body of the chat message.</param>
        public ChatMessage(string name, string msg)
        {
            this.Username = name;
            this.Message = msg;
        }

        public string ConvertToJSONString() 
        {
            return JsonConvert.SerializeObject(this, Formatting.None);
        }

        public static ChatMessage ConvertFromJSONString(string str) 
        {
            ChatMessage cm;
            cm = JsonConvert.DeserializeObject<ChatMessage>(str);
            return cm;
        }
    }



}
