﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using NetworkCommsDotNet;
using System.Net;
using NetworkCommsDotNet.Connections;
using System.IO;


namespace NetChatClient
{
    public partial class ChatForm : Form
    {
        string ServerName = "";
        string UserName = "";
        public ChatForm(string server, string user)
        {
            InitializeComponent();
            ServerName = Networking.ToIPAddress(server).ToString();
            UserName = user;

        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Environment.Exit(0);
            this.Close();
        }

        private void ChatForm_Load(object sender, EventArgs e)
        {
            NetworkComms.SendObject<string>("Join", ServerName, 2014, UserName);
            NetworkComms.AppendGlobalIncomingPacketHandler<string>("BroadcastMessage", PostMessage);
            textBox1.Focus();
        }

        private void PostMessage(PacketHeader header, Connection connection, string message)
        {
            ChatMessage cmessage = ChatMessage.ConvertFromJSONString(message);
            //MessageBox.Show(cmessage.Username + "\n" + cmessage.Message);
            listBox1.Invoke((MethodInvoker)delegate { listBox1.Items.Add(cmessage.Username + " :: " + cmessage.Message); });
            toolStripStatusLabel1.Text = "Last Post on "+ DateTime.Now.ToLongTimeString();
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Convert.ToInt32(e.KeyChar) == 13 && textBox1.Text.Length > 0)
            {
                ChatMessage cm = new ChatMessage(UserName, textBox1.Text);
                //MessageBox.Show(cm.ConvertToJSONString());
                NetworkComms.SendObject<string>("SendRequest", ServerName, 2014, cm.ConvertToJSONString());
                textBox1.Text = "";
                e.Handled = true;
            }
        }

        private void ChatForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            NetworkComms.SendObject<string>("Leave", ServerName, 2014, UserName);
            Environment.Exit(0);
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutBox ab = new AboutBox();
            ab.ShowDialog();
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveFileDialog1.ShowDialog();
        }

        private void saveFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            var f = saveFileDialog1.OpenFile();
            StreamWriter sw = new StreamWriter(f);
            foreach (string x in listBox1.Items) {
                sw.WriteLine(x);
            }
            sw.Close();
        }
    }
}
