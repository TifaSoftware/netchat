﻿using System;
using System.Net;
using System.Net.Sockets;

namespace NetChatClient
{
    /// <summary>
    /// Basic helper methods around networking objects (IPAddress, IpEndPoint, Socket, etc.)
    /// </summary>
    public static class Networking
    {
        /// <summary>
        /// Converts a string representing a host name or address to its <see cref="IPAddress"/> representation, 
        /// optionally opting to return a IpV6 address (defaults to IpV4)
        /// </summary>
        /// <param name="hostNameOrAddress">Host name or address to convert into an <see cref="IPAddress"/></param>
        /// <param name="favorIpV6">When <code>true</code> will return an IpV6 address whenever available, otherwise 
        /// returns an IpV4 address instead.</param>
        /// <returns>The <see cref="IPAddress"/> represented by <paramref name="hostNameOrAddress"/> in either IpV4 or
        /// IpV6 (when available) format depending on <paramref name="favorIpV6"/></returns>
        public static IPAddress ToIPAddress(string hostNameOrAddress)
        {
            var addrs = Dns.GetHostAddresses(hostNameOrAddress);
            return addrs[0];
        }
    }
}
    
