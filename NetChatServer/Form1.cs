﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Net;
using NetworkCommsDotNet;
using NetworkCommsDotNet.Connections;

namespace NetChatServer
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            OnStart();
            notifyIcon1.Visible = true;
            notifyIcon1.ShowBalloonTip(2000);
        }
        Dictionary<string, IPEndPoint> MailList = new Dictionary<string, IPEndPoint>(); 

        protected void OnStart()
        {
            
            NetworkComms.AppendGlobalIncomingPacketHandler<string>("SendRequest", BroadcastMessage);
            NetworkComms.AppendGlobalIncomingPacketHandler<string>("Join", AddClient);
            NetworkComms.AppendGlobalIncomingPacketHandler<string>("CheckName", CheckName);
            NetworkComms.AppendGlobalIncomingPacketHandler<string>("Leave", RemoveClient);
            try
            {
                Connection.StartListening(ConnectionType.TCP, new System.Net.IPEndPoint(System.Net.IPAddress.Any, 2014));
            }
            catch {
                MessageBox.Show("Port 2014 is already being used. The program will now close.");
                Environment.Exit(-1);
            }
            //this.Hide();
        }

        private void BroadcastMessage(PacketHeader header, Connection connection, string message)
        {
            foreach (IPEndPoint ip in MailList.Values) 
            {
                NetworkComms.SendObject<string>("BroadcastMessage", ip.ToString().Split(':')[0], ip.Port, message);

            }
        }

        private void AddClient(PacketHeader header, Connection connection, string message)
        {
            try
            {
                MailList.Add(message, (IPEndPoint)connection.ConnectionInfo.RemoteEndPoint);
            }
            catch {
                notifyIcon1.ShowBalloonTip(2000, "Warning", "Possible Hijack or Unofficial Client tried to send conflicting username", ToolTipIcon.Warning);
            }
        }

        private void CheckName(PacketHeader header, Connection connection, string message)
        {
            try
            {
                connection.SendObject<bool>("CheckNameR", !MailList.ContainsKey(message));
            }
            catch { }
        }

        private void RemoveClient(PacketHeader header, Connection connection, string message)
        {
            try
            {
                MailList.Remove(message);
            }
            catch { 
            
            }
        }

        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            notifyIcon1.ShowBalloonTip(2000);
        }

        private void notifyIcon1_BalloonTipShown(object sender, EventArgs e)
        {
            this.Hide();
        }
    }
}
